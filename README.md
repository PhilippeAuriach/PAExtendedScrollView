PAExtendedScrollView
====================

A simple class using UIScrollView to provide a scrollview showing part of previous and next view.
This class is based on the work made by Björn Sållarp. You can find his original blog post [here](http://blog.sallarp.com/iphone-ipad-appstore-like-uiscrollview-with-paging-and-preview/).

Basically, the result will look like this (this is ugly as hell for the example, but you can of course customize the views as you want):

![screenshot](http://philippeauriach.me/projects/imgs/PAExtendedScrollView/github_screenshot.png)

A complete working example is in the repo.
